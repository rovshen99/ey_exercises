from django.db import models


class FileData(models.Model):
    class Meta:
        db_table = 'FileData'
        managed = False
    id = models.AutoField(primary_key=True)
    path = models.TextField()
    sumOpeningActive = models.TextField()
    sumOpeningPassive = models.TextField()
    sumFlowDebit = models.TextField()
    sumFlowCredit = models.TextField()
    sumOutgoingActive = models.TextField()
    sumOutgoingPassive = models.TextField()


class Balance(models.Model):
    class Meta:
        db_table = 'Balance'
        managed = False
    id = models.AutoField(primary_key=True)

    classs = models.ForeignKey('ClassModel', on_delete=models.CASCADE)
    fileData = models.ForeignKey(FileData, on_delete=models.CASCADE)
    acc = models.TextField()
    openingActive = models.TextField()
    openingPassive = models.TextField()
    flowDebit = models.TextField()
    flowCredit = models.TextField()
    outgoingActive = models.TextField()
    outgoingPassive = models.TextField()


class ClassModel(models.Model):
    class Meta:
        db_table = 'Class'
        managed = False
    id = models.AutoField(primary_key=True)
    className = models.TextField()
    classOpeningActive = models.TextField()
    classOpeningPassive = models.TextField()
    classFlowDebit = models.TextField()
    classFlowCredit = models.TextField()
    classOutgoingActive = models.TextField()
    classOutgoingPassive = models.TextField()
