from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('read_file/', views.read_excel_file, name='read_file'),
    path('files_list/', views.files_list, name='files_list'),
    path('file_detail/<int:pk>/', views.file_detail, name='file_detail')
]