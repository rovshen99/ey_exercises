from django.shortcuts import render, HttpResponse
import pandas as pd
import sqlite3

from .models import FileData, Balance, ClassModel


def index(request):
    return render(request, 'excel/main.html')


def read_excel_file(request):
    if request.method == 'POST':
        # TODO: Try block
        input_file = request.POST.get('text', '')
        try:
            file = pd.read_excel(input_file, skiprows=list(range(0, 6)))\
                .rename(columns={'Б/сч': 'id', 'ВХОДЯЩЕЕ САЛЬДО': '1', 'Unnamed: 2': '2', 'ОБОРОТЫ': '3',
                                 'Unnamed: 4': '4', 'ИСХОДЯЩЕЕ САЛЬДО': '5', 'Unnamed: 6': '6'})
        except:
            return HttpResponse('Нет такого файла. <a href="/">Главное меню</a>')

        classes = []
        start = 1
        for index, row in file.iterrows():
            if pd.isnull(row['2']):
                end = index
                sliced = file.iloc[start: end:]
                classes.append(sliced.copy())
                start = end
            if index == len(file) - 1:
                sliced = file.iloc[start: index + 1:]
                classes.append(sliced.copy())

        # TODO: INSERT DATA INTO DATABASE.
        con = sqlite3.connect('db.sqlite3')
        cur = con.cursor()
        cur.execute("INSERT INTO FileData(path, sumOpeningActive, sumOpeningPassive, sumFlowDebit, "
                    "sumFlowCredit, sumOutgoingActive, sumOutgoingPassive) VALUES (?,?,?,?,?,?,?)",
                    (input_file, classes[-1]['1'].iloc[-1], classes[-1]['2'].iloc[-1], classes[-1]['3'].iloc[-1],
                     classes[-1]['4'].iloc[-1], classes[-1]['5'].iloc[-1], classes[-1]['6'].iloc[-1]))

        for index, df in enumerate(classes):
            if not df.empty:
                if index == len(classes)-1:
                    cur.execute("INSERT INTO Class(className, classOpeningActive, classOpeningPassive, classFlowDebit, "
                                "classFlowCredit, classOutgoingActive, classOutgoingPassive) VALUES (?, ?,?,?,?,?,?)",
                                (df['id'].iloc[0], df['1'].iloc[-2], df['2'].iloc[-2], df['3'].iloc[-2], df['4'].iloc[-2],
                                 df['5'].iloc[-2], df['6'].iloc[-2]))

                else:
                    cur.execute("INSERT INTO Class(className, classOpeningActive, classOpeningPassive, classFlowDebit, "
                                "classFlowCredit, classOutgoingActive, classOutgoingPassive) VALUES (?,?,?,?,?,?,?)",
                                (df['id'].iloc[0], df['1'].iloc[-1], df['2'].iloc[-1], df['3'].iloc[-1], df['4'].iloc[-1],
                                 df['5'].iloc[-1], df['6'].iloc[-1]))

                cur.execute("SELECT id FROM Class ORDER BY ID DESC LIMIT 1")
                classid = cur.fetchone()
                cur.execute("SELECT id FROM FileData ORDER BY ID DESC LIMIT 1")
                fileDataid = cur.fetchone()

                for index2, row in df.iloc[1:-1].iterrows():
                    if index2 < len(file)-2:
                        cur.execute("INSERT INTO Balance(classs_id, fileData_id, acc, openingActive, openingPassive, "
                                    "flowDebit, flowCredit, outgoingActive, outgoingPassive) VALUES (?,?,?,?,?,?,?,?,?)",
                                    (classid[0], fileDataid[0], row['id'], row['1'], row['2'], row['3'], row['4'],
                                     row['5'], row['6']))

        con.commit()
        con.close()
        return HttpResponse("""Успешно <a href="/">Главное меню</a>""")


def files_list(request):
    files = FileData.objects.all()
    return render(request, 'excel/list.html', context={'files': files})


def file_detail(request, pk):
    file = FileData.objects.get(id=pk)
    file_balances = Balance.objects.filter(fileData_id=file.id)
    file_classes = set(ClassModel.objects.filter(balance__in=file_balances.all()))
    return render(request, 'excel/detail.html', context={'file': file, 'file_balances': set(file_balances),
                                                         'file_classes': file_classes})

