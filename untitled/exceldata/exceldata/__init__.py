import sqlite3
# TODO: CREATE TABLES IF IT'S NOT EXISTS
con = sqlite3.connect('db.sqlite3')
cur = con.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS Class(id INTEGER primary key, className TEXT, classOpeningActive TEXT,"
            " classOpeningPassive TEXT, classFlowDebit TEXT, classFlowCredit TEXT, classOutgoingActive TEXT, "
            "classOutgoingPassive TEXT)")

cur.execute("CREATE TABLE IF NOT EXISTS FileData(id INTEGER PRIMARY KEY, path TEXT, sumOpeningActive TEXT, "
            "sumOpeningPassive TEXT, sumFlowDebit TEXT, sumFlowCredit TEXT, sumOutgoingActive TEXT, "
            "sumOutgoingPassive TEXT)")

cur.execute("CREATE TABLE IF NOT EXISTS Balance(id INTEGER primary key, classs_id INTEGER, fileData_id INTEGER, "
            "acc TEXT, openingActive TEXT, openingPassive TEXT, flowDebit TEXT, flowCredit TEXT, "
            "outgoingActive TEXT, outgoingPassive TEXT, FOREIGN KEY (classs_id) REFERENCES Class(id),"
            "FOREIGN KEY (fileData_id) REFERENCES FileData(id))")