import random
from datetime import datetime, timedelta
import string
import sqlite3
import codecs
import sys


# Randomizers
def random_date_generate():
    end = datetime.now()
    start = end - timedelta(days=5*365.25)
    random_date = start + (end-start) * random.random()
    return random_date.strftime('%d.%m.%Y')


def random_latin_str_generate():
    return ''.join([random.choice(string.ascii_letters) for n in range(10)])


def random_cyrillic_str_generate():
    return ''.join([random.choice(''.join([chr(x) for x in range(1040, 1104)]) + "Ёё") for n in range(10)])


def random_number_generate():
    return random.randint(1, 100000000)


def random_float_number_generate():
    return '{:.8f}'.format(random.uniform(1, 20))


def random_data_line():
    return '{}||{}||{}||{}||{}||\n'.format(random_date_generate(), random_latin_str_generate(),
                                         random_cyrillic_str_generate(), random_number_generate(),
                                         random_float_number_generate())


# 1.Generating 100 files.
def generate_files():
    print("Generating {} files".format(files_count))
    for i in range(files_count):
        with codecs.open('file%d.txt' % i, 'w', 'utf-8') as f:
            for _ in range(100000):
                f.write(random_data_line())


# 2.Combine files into one and remove line if it has input string
def combine_files():
    removed_lines_count = 0
    remove_string = input('Enter the string what you want to remove from file: ')
    with codecs.open('united.txt', 'w', 'utf-8') as uni:
        for i in range(files_count):
            with codecs.open('file%d.txt' % i, 'r+', 'utf-8') as f:
                lines = f.readlines()
                f.seek(0)
                for line in lines:
                    if remove_string not in line:
                        f.write(line)
                        uni.write(line)
                    else:
                        removed_lines_count += 1
                f.truncate()

        print('Removed {} lines'.format(removed_lines_count))


# 3.Create a Table in sqlite3 and import data with process of progress
def import_data_in_db():
    con = sqlite3.connect('data.db')
    cur = con.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS Lines(random_date TEXT, latin_symbols TEXT, "
                "cyrillic_symbols TEXT, int_number int, float_number REAL)")

    with codecs.open('united.txt', 'r', 'utf-8') as f:
        data = [row.split('||') for row in f.read().splitlines()]
        data = [row[:-1] for row in data]
        for imported, row in enumerate(data, start=1):
            cur.execute("INSERT INTO Lines (random_date, latin_symbols, cyrillic_symbols, int_number, float_number)"
                            " VALUES(?,?,?,?,?);", row)
            print(f'Imported {imported}, Left {len(data)-imported}')
    con.commit()
    con.close()


# 4. SUM AND MEDIAN WITH SQL REQUEST
def calculate_sum_and_median_with_sql_request():
    con = sqlite3.connect('data.db')
    cur = con.cursor()
    cur.execute("SELECT SUM(int_number) FROM Lines")
    print('SUM of the integer numbers:', *cur.fetchone())
    cur.execute("""SELECT AVG(float_number)
        FROM (SELECT float_number
             FROM Lines
             ORDER BY float_number
             LIMIT 2 - (SELECT COUNT(*) FROM Lines) % 2    -- odd 1, even 2
             OFFSET (SELECT (COUNT(*) - 1) / 2
                     FROM Lines));""")
    print("Median of float numbers:", *cur.fetchone())
    con.commit()
    con.close()


# Global variables.
files_count = 5    # ---> 5 т.к 100 файлов генерируются очень долго

if __name__ == '__main__':
    generate_files()
    print('1 - Combine files, 2 - Import data into database, 3 - Calculate Sum and median, 0 - Exit')
    inp = input("Choose: ")
    while inp != '0':
        if inp == '1': combine_files()
        elif inp == '2': import_data_in_db()
        elif inp == '3': calculate_sum_and_median_with_sql_request()
        elif inp == '0': sys.exit()
        else:
            print('Вы неправильно ввели')
        print('1 - Combine files, 2 - Import data into database, 3 - Calculate Sum and median, 0 - Exit')
        inp = input("Choose: ")
